online-bookstore

author
  first_name varchar(100)
  last_name varchar(100)
  street varchar(50)
  city varchar(100)
book
  title varchar(100)
  description varchar(200)
  long_description text
book_author
  book [1 to many] author
book_comment
  comment text
  send_ts timestamp
  book_rating
  customer
customer
  first_name varchar(80)
  last_name varchar(100)
  street varchar(50)
  city varchar(90)
purchase
  customer
  status
purchaes_item
  book
  quantity
purchase_status
  name


http://www.vertabelo.com/blog/technical-articles/7-common-database-design-errors