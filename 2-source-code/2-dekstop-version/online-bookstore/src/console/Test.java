package console;

import java.util.ArrayList;
import java.util.List;
import model.dao.ob.AuthorDao;
import model.dao.ob.BookDao;
import model.obj.ob.Author;
import model.obj.ob.Book;

public class Test {

    public static void main(String[] args) {
        //Author
        Author author = new Author();
        author.setFirstName("Yosua");
        author.setLastName("Suheru");
        AuthorDao authorDao = new AuthorDao();
        authorDao.save(author);
        author = (Author) authorDao.get(0);
        System.out.println(author.getFirstName() + " " + author.getLastName());
        //Book
        Book book = new Book();
        book.setTitle("Framework Fundamental: Generic");
        BookDao bookDao = new BookDao();
        bookDao.save(book);
        book = bookDao.get(0);
        System.out.println(book.getTitle());
        //List
        List list = new ArrayList();
        list.add(author);
        list.add(book);
        for (Object object : list) {
            System.out.println(((Book) object).getTitle());
        }
    }
}