package model.dao.ob;

import java.util.ArrayList;
import java.util.List;

public abstract class OnlineBookstore1Dao<T> {

    private List<T> collections = new ArrayList();

    public boolean save(T object) {
        return collections.add(object);
    }

    public T get(int id) {
        return collections.get(id);
    }

    public boolean delete(T object) {
        return collections.remove(object);
    }
}